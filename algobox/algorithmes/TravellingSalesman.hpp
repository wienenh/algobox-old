//
//  TravellingSalesman.hpp
//  algobox
//
//  Created by Hans Wienen on 02/04/2020.
//  Copyright © 2020 Hans Wienen. All rights reserved.
//

#ifndef TravellingSalesman_hpp
#define TravellingSalesman_hpp

#include <string>
#include "ViewController.h"

struct location {
    double x;
    double y;
};

class TravellingSalesman {
    int number_of_cities;
    location * cities;
    double * distances;
    ViewController *delegate = nullptr;
    int index(int i, int j) { return i * number_of_cities + j; }
    
public:
    bool load_file(std::string fileName);
    void calculateDistances();
    bool register_observer(ViewController *);
    void setDistance(int i, int j, double d) { distances[index(i,j)] = d;}
    double getDistance(int i, int j) {return distances[index(i,j)];}
    double getHeuristicTour();
};

#endif /* TravellingSalesman_hpp */
