//
//  TravellingSalesman.cpp
//  algobox
//
//  Created by Hans Wienen on 02/04/2020.
//  Copyright © 2020 Hans Wienen. All rights reserved.
//

#include "TravellingSalesman.hpp"
#include <iostream>
#include <fstream>


bool TravellingSalesman::register_observer(ViewController *d) {
    delegate = d;
    return true;
}

bool TravellingSalesman::load_file(std::string filename) {
        
    std::ifstream myfile(filename);
    
    if (!myfile.good()) {
        std::cerr << "File not found:" << filename << "!" << std::endl;
        return false;
    }

    myfile >> number_of_cities;
    
    cities = new location[number_of_cities];
    distances = new double[number_of_cities * number_of_cities];
    int city = 0;
    int city_label;
    
    double x, y;
    while ( myfile >> city_label && !myfile.eof()){
        myfile >> x;
        myfile >> y;
        location l = {x, y};
        cities[city] = l;
        city++;
    }

    return true;
}

void TravellingSalesman::calculateDistances() {
    
    for (int i = 0; i < number_of_cities; i++) {
        setDistance(i, i, 0);

        for (int j = i+1; j < number_of_cities; j++) {
            location l = cities[i];
            location m = cities[j];
            double d = sqrt((l.x - m.x)*(l.x - m.x) + (l.y - m.y)*(l.y - m.y));
            setDistance(i, j, d);
            setDistance(j, i, d);
        }
    }
}

double TravellingSalesman::getHeuristicTour() {
    double costs = 0;
    bool visited[number_of_cities];
    for (int i = 0; i < number_of_cities; i++) visited[i] = false;
    
    visited[0] = true;

    int city = 0;
    for (int i = 1; i < number_of_cities; i++) {
        double one_leg = std::numeric_limits<double>::infinity();
        int candidate = -1;
        for (int j = 0; j < number_of_cities; j++) {
            double candidate_distance = getDistance(city, j);
            if ( candidate_distance < one_leg && !visited[j] && candidate_distance > 0){
                one_leg = candidate_distance;
                candidate = j;
            }
        }
        costs += one_leg;
        city = candidate;
        visited[candidate] = true;
    }
    
    costs += getDistance(city, 0);
    return costs;
}
