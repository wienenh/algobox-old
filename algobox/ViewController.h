//
//  ViewController.h
//  algobox
//
//  Created by Hans Wienen on 02/04/2020.
//  Copyright © 2020 Hans Wienen. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController
- (IBAction)openFileButton:(id)sender;
@property (weak) IBOutlet NSTextField *fileLabel;
@property (weak) IBOutlet NSTextField *fileNameTextField;
@property (weak) IBOutlet NSTextField *progressField;
@property (weak) IBOutlet NSProgressIndicator *progressBar;

- (void) updateCounter:(double)value;
- (void) setProgressLimitsLow:(double) min High:(double) max;
- (void) setLabelText: (NSString *) string;



@end

