//
//  ViewController.m
//  algobox
//
//  Created by Hans Wienen on 02/04/2020.
//  Copyright © 2020 Hans Wienen. All rights reserved.
//

#import "ViewController.h"
#include "algorithmes/TravellingSalesman.hpp"
#include <assert.h>
#include <pthread.h>

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.progressBar setCanDrawConcurrently:YES];
    [self.progressBar setUsesThreadedAnimation:YES];

    // Do any additional setup after loading the view.
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

void * PosixThreadMainRoutine(void * data) {
    NSLog(@"Thread started...");
    ((TravellingSalesman *) data)->calculateDistances();
    NSLog(@".. and completed!");
    return NULL;
}

- (IBAction)openFileButton:(id)sender {
    
    NSOpenPanel* openDialog = [NSOpenPanel openPanel];
    [openDialog setCanChooseFiles:YES];
    [openDialog setCanChooseDirectories:YES];
    [openDialog setAllowsMultipleSelection:NO];
    if ( [openDialog runModal] == NSModalResponseOK ) {
        NSArray *urls = [openDialog URLs];
        NSURL *url = [urls objectAtIndex:0];
        [self.fileNameTextField setStringValue:[url lastPathComponent]];
        std::string file_name = [url fileSystemRepresentation];
        TravellingSalesman * t = new TravellingSalesman();
        t->register_observer(self);
        t->load_file(file_name);
        // Start the calculation asynchornously:
        pthread_attr_t  attr;
        pthread_t       posixThreadID;
        int             returnVal;
        
        returnVal = pthread_attr_init(&attr);
        assert(!returnVal);
        returnVal = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        assert(!returnVal);
        
        int threadError = pthread_create(&posixThreadID, &attr, &PosixThreadMainRoutine, t);
        
        // t->calculateDistances();
        // double shortest_distance = t->getHeuristicTour();
        // [self.progressField setDoubleValue:shortest_distance];
        [self.progressField setStringValue:@"Now waiting for thread"];
        
    }
}
@end
