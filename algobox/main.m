//
//  main.m
//  algobox
//
//  Created by Hans Wienen on 02/04/2020.
//  Copyright © 2020 Hans Wienen. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
    }
    return NSApplicationMain(argc, argv);
}
