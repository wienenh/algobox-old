//
//  algoboxTests.m
//  algoboxTests
//
//  Created by Hans Wienen on 02/04/2020.
//  Copyright © 2020 Hans Wienen. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "TravellingSalesman.hpp"

@interface algoboxTests : XCTestCase

@end

@implementation algoboxTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testDistanceCalculation {
    TravellingSalesman * t = new TravellingSalesman();
    bool loaded = t->load_file("/Users/hanswienen/tmp/wk3/test.txt");
    XCTAssertTrue(loaded);
    t->calculateDistances();
    double epsilon = 1e-2;
    double distances[] {0, sqrt(5), sqrt(65), sqrt(10), 0, sqrt(34), sqrt(5), 0, sqrt(37), 0};
    int c1[] {0, 1, 2, 3, 1, 2, 3, 2, 3, 3};
    int c2[] {0, 0, 0, 0, 1, 1, 1, 2, 2, 3};
    for (int i = 0; i < 10; i++) {
        double cdist = t->getDistance(c1[i], c2[i]);
        double rdist = distances[i];
        XCTAssertEqualWithAccuracy(cdist, rdist, epsilon,"Distance between %i and %i is %f instead of %f", c1[i], c2[i], cdist, rdist);
    }
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
